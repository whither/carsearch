Coding Challenge

This project implements the mobile.de coding challenge


Architecture
====
Due to the small scope the application has been implemented as a monolith using the ratpack library.
Instead of directly serving the html, a single page frontend application and a json backend could have been used.
Instead, the application has an old-fashioned html interface which is dynamically generated and has no user interface design.
This is mainly due to time constraints, but serves to demonstrate the required functionality.
For production, horizontal scalability is easy, a seperate database backend is recommended.

Running the project
====
gradlew flywayMigrate run runs the webapp which can be reached at http://localhost:5050

Tests
=====
You can find the acceptance tests in src/browserTest/groovy/DetailSpec.groovy and src/browserTest/groovy/SearchSpec.groovy

For the execution of the acceptance tests via IntelliJ IDEA or gradlew an installed Firefox is required. If you wish to
use another browser, please modify src/browserTest/GebConfig.groovy and add the respective dependency to the build.gradle.

Because of the small scope and little isolated code base, there are no further tests. The project has been implemented using the
Acceptance Test Driven Design approach and can be understood as a stable, prototype. Production readiness would require
solving the open issues (see below).

Tests can be run with gradlew browserTest

Database
=====
The database schema can be found at src/main/resources/db.migration. The sql scripts there are compatible with the H2 database engine,
which is used for development and testing.
The usage of another database engine for production would require:
- addition of jdbc driver dependency to build.gradle
- configuration of database via system-properties or an additional configuration file for production

Configuration
====
Ratpack offers a configuration mechanism which is used for configuration of development environment.
Currently, development configuration is done via /src/main/resources/application.properties
This mechanism would have to be extended to include production configuration e.g. from /etc/carsearch.conf or similar files

Monitoring
====
Monitoring is important for production, but has not been included due to time constraints. Ratpack offers excellent
integration of Dropwizard Metrics.

Logging
====
Logging would have to be configured for production.
A catch all handler has been defined in Ratpack.groovy which adds request logging with unique request ids.

Build shippable artifact
=====
gradlew shadowJar produces an executable fat jar which contains the complete application in build/libs/coding-challenge-all.jar
For production purposes, packaging and creation of a service script would be advisable.

Open Issues
=====
Due to time constraints, the following open issues exist:
- there is no user interface design
- monitoring and logging are not implemented
- as of now, the search is case sensitive
- There is no input or output validation
- There are no security measures like https or injection prevention
- images are statically shipped but should probably be shipped from dedicated machine or a CDN.
- Database operations are not in blocking thread, violating ratpack architecture
- database operations should be optimised, connections pooled.
- ratpack offers great hystrix integration which should possibly be used for the database interaction

Time needed for implementation
====
I have not previously used Flyway, Groovy SQL nor Geb. Development time has been ~9 hours.