package com.ffoerster.carsearch

import groovy.transform.Immutable

@Immutable
class Config {
    String jdbcDriver
    String connectionString
}
