package com.ffoerster.carsearch

import groovy.sql.Sql

class CarSearch {

    List findBy(Map<String, String> criteria, config) {
        def query = "SELECT * from CARS where color LIKE '%${criteria.color}%' AND make LIKE '%${criteria.make}%' AND model LIKE '%${criteria.model}%';".toString()
        executeQuery(query, config)
    }

    Map findCar(String id, config) {
        def query = "SELECT * from CARS where id = ${id};"
        executeQuery(query, config)[0]
    }

    private List executeQuery(String query, config) {
        def sql = Sql.newInstance(config.connectionString, '', '', config.jdbcDriver)
        List rows = sql.rows(query)
        sql.close()
        rows
    }

}
