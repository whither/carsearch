CREATE TABLE cars (
    ID int not null,
    COLOR varchar(30) not null,
    MAKE varchar(30) not null,
    MODEL varchar(30) not null,
    FEATURES varchar(30) not null,
    PRICE int not null,
    IMAGE varchar(80) not null,
);