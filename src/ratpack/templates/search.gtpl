package templates

yieldUnescaped '<!DOCTYPE html>'
html {
    head {
        meta(charset: 'utf-8')
        title("Search")

    }
    body {
        header {
            h1 'Search'
        }

        section {
            h2 title
            includeGroovy '_search_form.gtpl'
        }

        footer {}
    }
}
