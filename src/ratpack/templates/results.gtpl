package templates

yieldUnescaped '<!DOCTYPE html>'
html {
    head {
        meta(charset: 'utf-8')
        title("Results")

    }
    body {
        header {
            h1 'Search Result'
        }

        section {
            h2 'Your search:'
            div("color: ${params.color}")
            div("make: ${params.make}")
            div("model: ${params.model}")
            div{
                a(href:"/?make=${params.make}&model=${params.model}&color=${params.color}", 'refine search')
            }
            h2 'Your Results:'
            if (resultSet.empty) {
                div(class:'error', 'sorry, no cars found')
            }
            resultSet.each { values ->

                div(class: 'result'){
                    img(src:"${values.image}")
                    span(class:'color', values.color)
                    span(class:'make', values.make)
                    span(class:'model', values.model)
                    span(class:'price', values.price)
                    a(class:'detailsLink', href:"cars/${values.id}", 'details')
                }
            }

        }
    }
}
