package templates

yieldUnescaped '<!DOCTYPE html>'
html {
    head {
        meta(charset: 'utf-8')
        title("Details")

    }
    body {
        header {
            h1 'Details'
        }

        section {
                div(class: 'result'){
                    img(src:"${result.image}")
                    span(class:'color', result.color)
                    span(class:'make', result.make)
                    span(class:'model', result.model)
                    span(class:'features', result.features)
                    span(class:'price', result.price)
                }
            }

        }
    }
