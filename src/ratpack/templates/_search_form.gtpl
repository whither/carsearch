package templates

form(name: 'search-form', action: 'search', method: 'get') {
    label(for: 'make', 'Make: ')
    input(type: 'text', name: 'make', maxlength: '30', value: presetValue(params, 'make'))
    label(for: 'model', 'Model: ')
    input(type: 'text', name: 'model', maxlength: '30', value: presetValue(params, 'model'))
    label(for: 'color', 'Color: ')
    input(type: 'text', name: 'color', maxlength: '30', value: presetValue(params, 'color'))
    input(type: 'submit', name: 'submitButton')
}

String presetValue(Map<String, String> params, String key) {
    String value = params.get(key)
    "${value != "null" && value != null ? value : ''}"
}