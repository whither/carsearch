import com.ffoerster.carsearch.CarSearch
import com.ffoerster.carsearch.Config
import ratpack.config.ConfigData
import ratpack.groovy.template.MarkupTemplateModule
import ratpack.handling.RequestId

import static ratpack.groovy.Groovy.groovyMarkupTemplate
import static ratpack.groovy.Groovy.ratpack

ratpack {

    bindings {
        ConfigData configData = ConfigData.of()
                .props(ClassLoader.getSystemResource('application.properties') as URL)
                .sysProps()
                .env()
                .build()

        bindInstance(Config, configData.get(Config))
        bindInstance(CarSearch, new CarSearch())
        module MarkupTemplateModule
    }


    handlers {
        handler(RequestId.bindAndLog())
        get('') {
            def queryParams = request.queryParams
            Map<String, String> params = [make: queryParams.make, model: queryParams.model, color: queryParams.color]
            render groovyMarkupTemplate("search.gtpl", params: params)
        }
        get('search') { CarSearch carSearch, Config config ->
            def queryParams = request.queryParams
            Map<String, String> params = [make: queryParams.make, model: queryParams.model, color: queryParams.color]

            render groovyMarkupTemplate("results.gtpl", resultSet: carSearch.findBy(params, config), params: params)
        }
        get('cars/:id?') { CarSearch carSearch, Config config ->
            render groovyMarkupTemplate("details.gtpl", result: carSearch.findCar(pathTokens.id, config))

        }
        assets "public"
    }
}
