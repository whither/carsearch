import geb.spock.GebReportingSpec
import pages.ResultsPage
import pages.SearchPage
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.ApplicationUnderTest
import spock.lang.Shared
import spock.lang.Unroll

class SearchSpec extends GebReportingSpec {

    @Shared
    ApplicationUnderTest aut = new GroovyRatpackMainApplicationUnderTest()

    def setup() {
        URI base = aut.address
        browser.baseUrl = base.toString()
    }

    static Map ford = [color: 'Persimmon red', make: 'Ford', model: 'Mustang', image: '/images/ford.jpg', price: 32000]
    static Map buick = [color: 'Midnight blue', make: 'Buick', model: 'Lacrosse', image: '/images/buick.jpg', price: 29000]
    static Map bmw = [color: 'Ocean blue', make: 'BMW', model: 'X5', image: '/images/bmw.jpg', price: 79000]
    static Map audi_green = [color: 'British racing green', make: 'Audi', model: 'A4', image: '/images/audi_green.jpg', price: 6700]
    static Map audi_red = [color: 'Scarlet red', make: 'Audi', model: 'A4', image: '/images/audi_red.jpg', price: 4500]

    def "index page is search page"() {
        when:
        to SearchPage

        then:
        at SearchPage
    }

    def 'should return to search page with pre-filled values if "refine search" link is clicked'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        and: 'I execute the search'
        searchForm.model = 'A4'
        searchForm.make = 'some make'
        searchForm.color = 'orange'
        searchButton.click()

        when: 'I click on the "refine search" link'
        at ResultsPage
        refineLink.click()

        then: 'I am back at the search page'
        at SearchPage

        and: 'the form is pre-filled with my previous search parameters'
        searchForm.model == 'A4'
        searchForm.make == 'some make'
        searchForm.color == 'orange'

    }

    def 'should show image, make, model, color and price for multiple results'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: 'I execute the search'
        searchForm.model = 'A4'
        searchButton.click()

        then: 'image, make, model, color and price are shown for each result'
        results.contains(audi_green)
        results.contains(audi_red)
    }

    @Unroll("Should return #expectedResults for search for #make")
    def 'search for make'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: "I search for make"
        searchForm.make = make

        and: 'I execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: "showing cars by the specified maker"
        results.containsAll(expectedResults)

        and: 'no other results'
        results.size() == expectedResults.size()

        where:
        make    | expectedResults
        'Ford'  | [ford]
        'Buick' | [buick]
        'BMW'   | [bmw]
        'Audi'  | [audi_green, audi_red]
    }

    @Unroll("Should return #expectedResults for search for #model")
    def 'search for model'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when:
        'I search for model'
        searchForm.model = model

        and: 'I execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: 'showing cars of the specified model'
        results.containsAll(expectedResults)

        and: 'no other results'
        results.size() == expectedResults.size()

        where:
        model      | expectedResults
        'Mustang'  | [ford]
        'Lacrosse' | [buick]
        'X5'       | [bmw]
        'A4'       | [audi_green, audi_red]
    }

    @Unroll("Should return #expectedResults for search for #make #model")
    def 'search for make and model'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: 'I search for make and model'
        searchForm.make = make
        searchForm.model = model

        and: 'I execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: 'showing the cars by specified maker and of specified model'
        results.containsAll(expectedResults)

        and: 'no other results'
        results.size() == expectedResults.size()

        where:
        make    | model      | expectedResults
        'Ford'  | 'Mustang'  | [ford]
        'Buick' | 'Lacrosse' | [buick]
        'BMW'   | 'X5'       | [bmw]
        'Audi'  | 'A4'       | [audi_green, audi_red]
    }

    @Unroll("Should return #expectedResults for search for #make #model")
    def 'search for color'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: 'I search for color'
        searchForm.color = color

        and: 'I execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: 'showing cars of the specified color'
        results.containsAll(expectedResults)

        and: 'no other results'
        results.size() == expectedResults.size()

        where:
        color   | expectedResults
        'red'   | [ford, audi_red]
        'green' | [audi_green]
        'blue'  | [bmw, buick]
    }

    def 'should return "sorry, no cars found" for search for Trabant'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: 'I input Trabant'
        searchForm.make = 'Trabant'

        and: 'I execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: 'showing "sorry, no cars found"'
        $('div.error').text() == 'sorry, no cars found'

        and: 'no other results'
        results.size() == 0
    }

    def 'should return all cars for search for blank inputs'() {
        given: 'is the default data set and an opened search specification page'
        to SearchPage

        when: 'I leave all inputs blank and execute the search'
        searchButton.click()

        then: 'Then the web application shows a search result page'
        at ResultsPage

        and: "showing all cars"
        results.containsAll([ford, bmw, buick, audi_green, audi_red])
    }

    List getResults() {
        $('div.result').collect { parseResult(it) }
    }

    Map parseResult(def result) {
        [
                image: URI.create(result.$('img').getAttribute('src')).path,
                color: result.$('span.color').text(),
                make : result.$('span.make').text(),
                model: result.$('span.model').text(),
                price: Integer.valueOf(result.$('span.price').text())
        ]
    }
}