package pages

import geb.Page

class DetailsPage extends Page {

    static at = { title == "Details" }

    static content = {
        result{$('div.result')}
        image{URI.create(result.$('img').getAttribute('src')).path}
        color{result.$('span.color').text()}
        make{result.$('span.make').text()}
        model{result.$('span.model').text()}
        features{result.$('span.features').text()}
        price{result.$('span.price').text()}
    }
}