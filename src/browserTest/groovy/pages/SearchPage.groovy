package pages

import geb.Page

class SearchPage extends Page {

    static url = "/"

    static at = { title == "Search" }

    static content = {
        searchForm {
            $('form', name: 'search-form')
        }
        searchButton(to: ResultsPage){
            $('input', type: 'submit')
        }
    }
}