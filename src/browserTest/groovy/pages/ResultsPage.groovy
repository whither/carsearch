package pages

import geb.Page

class ResultsPage extends Page {

    static url = "/search"

    static at = { title == "Results" }

    static content = {
        resultSet{$('div', 'results')}
        refineLink{$('a')}
    }
}