import geb.spock.GebReportingSpec
import pages.DetailsPage
import pages.SearchPage
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.ApplicationUnderTest
import spock.lang.Shared
import spock.lang.Unroll

class DetailSpec extends GebReportingSpec {

    @Shared
    ApplicationUnderTest aut = new GroovyRatpackMainApplicationUnderTest()

    def setup() {
        URI base = aut.address
        browser.baseUrl = base.toString()
    }


    static Map cars = [
            1: [color: 'Persimmon red', make: 'Ford', model: 'Mustang', features: 'convertable', image: '/images/ford.jpg', price: 32000],
            2: [color: 'Midnight blue', make: 'Buick', model: 'Lacrosse', features: 'limousine, Xenon headlight', image: '/images/buick.jpg', price: 29000, id: 2]
    ]

    def "all search results should contain a link to their details"() {
        given: 'the search page'
        to SearchPage

        when: 'I execute a search for all cars'
        searchButton.click()

        then: 'all results contain a link labelled "details"'
        $('div.result').each {
            assert it.$('a.detailsLink').text() == 'details'
        }
    }

    def "details link should link to correct details page"() {
        given: 'the search page'
        to SearchPage

        when: 'I execute a search for all cars'
        searchButton.click()
        List<String> hrefs = $('div.result').collect {
            it.$('a.detailsLink').getAttribute('href')
        }

        then: 'all results contain the correct details link'
        hrefs == (1..5).collect { "${aut.address}cars/${it}" }
    }

    @Unroll("the details page for carId #carId should contain all values")
    def "the details page should contain all values"() {
        given: 'the search page'
        to SearchPage

        when: 'I execute a search for all cars'
        searchButton.click()

        and: 'I click on the details link of a car'
        $('div.result').$('a.detailsLink', href: "${aut.address}cars/${carId}").click()

        then: 'I am at the details page'
        at DetailsPage

        and: 'the details page contains all details'
        make == cars[carId].make
        model == cars[carId].model
        color == cars[carId].color
        price == cars[carId].price.toString()
        image == cars[carId].image
        features == cars[carId].features

        where:
        carId << [1, 2]
    }
}